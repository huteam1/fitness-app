import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class CoachTab extends Tab {
    private CoachData[] coaches;
    private VBox content;
    private Label currentCoachLabel;
    private Label currentCoachInfoLabel;
    Stage primaryStage;

    public CoachTab(Stage primaryStage) {
        super("Coaches");
        this.primaryStage = primaryStage;
        coaches = new CoachData[] {
                new CoachData("Kees", "Babbelaar", 35, "Zwemcoach", "Kees.Babbelaar@hotmail.com", "0652817495"),
                new CoachData("Laura", "van Dijk", 28, "Fitness coach", "Laura.van.Dijk@hotmail.com", "0685947612"),
                new CoachData("Wesley", "Sneijder", 38, "Hardloop coach", "Wesley.Sneijder@hotmail.com", "0685947325"),
                new CoachData("Jans", "Leerdam", 32, "Fiets coach", "Jans.Leerdam@hotmail.com", "0695725465")
        };
        setContent(createContent());
    }

    private VBox createContent() {
        content = new VBox();
        content.setSpacing(10);

        Label titleLabel = new Label("Coaches");
        titleLabel.setAlignment(Pos.TOP_CENTER);
        titleLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

        currentCoachInfoLabel = new Label("Click a coach to set it as current coach (will be highlighted in green)!");
        content.getChildren().addAll(titleLabel, currentCoachInfoLabel);

        currentCoachLabel = new Label("Current Coach: ");
        content.getChildren().add(currentCoachLabel);

        for (CoachData coachData : coaches) {
            Label nameLabel = new Label("Name: " + coachData.getName());
            Label surnameLabel = new Label("Surname: " + coachData.getSurname());
            Label ageLabel = new Label("Age: " + coachData.getAge());
            Label typeLabel = new Label("Type: " + coachData.getType());
            Label emailLabel = new Label("Email: " + coachData.getEmail());
            Label phoneNumberLabel = new Label("Phone Number: " + coachData.getPhoneNumber());

            VBox coachBox = new VBox(nameLabel, surnameLabel, ageLabel, typeLabel, emailLabel, phoneNumberLabel);
            coachBox.setStyle("-fx-border-color: #CCCCCC; -fx-border-width: 1px; -fx-padding: 5px;");

            coachBox.setOnMouseClicked(event -> {
                showCoachInformation(coachData);
                highlightCoach(coachBox);
            });

            content.getChildren().add(coachBox);
        }

        return content;
    }

    private void showCoachInformation(CoachData coachData) {
        currentCoachLabel.setText("Current Coach: " + coachData.getName());
        // Display other coach information as desired
    }

    private void highlightCoach(VBox coachBox) {
        // Reset background color of all coach boxes
        for (Node node : content.getChildren()) {
            if (node instanceof VBox) {
                VBox box = (VBox) node;
                box.setBackground(Background.EMPTY);
            }
        }

        // Set background color of selected coach box to green
        coachBox.setBackground(new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, null)));
    }
}
