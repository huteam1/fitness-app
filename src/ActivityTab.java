import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ActivityTab extends Tab {
    Stage primaryStage;
    ObservableList<ActivityEntry> activityEntries = FXCollections.observableArrayList();
    InputHandler inputHandler = new InputHandler();

    public ActivityTab(Stage primaryStage) {
        super("Activities");
        this.primaryStage = primaryStage;
        setContent(createContent());
    }

    private VBox createContent() {
        VBox content = new VBox();
        content.setAlignment(Pos.CENTER);

        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(10));

        // Create ImageViews for the pictures
        ImageView walkingImage = new ImageView(new Image("img/walking.png"));
        ImageView runningImage = new ImageView(new Image("img/run.png"));
        ImageView cyclingImage = new ImageView(new Image("img/cycling.png"));
        ImageView swimmingImage = new ImageView(new Image("img/swimming.png"));

        // Set the preferred sizes for the ImageViews
        walkingImage.setFitWidth(150);
        walkingImage.setFitHeight(150);
        runningImage.setFitWidth(150);
        runningImage.setFitHeight(150);
        cyclingImage.setFitWidth(150);
        cyclingImage.setFitHeight(150);
        swimmingImage.setFitWidth(150);
        swimmingImage.setFitHeight(150);

        // Create buttons with images and text
        Button walkingButton = createActivityButton("Walking", walkingImage);
        Button runningButton = createActivityButton("Running", runningImage);
        Button cyclingButton = createActivityButton("Cycling", cyclingImage);
        Button swimmingButton = createActivityButton("Swimming", swimmingImage);

        // Set button styles
        walkingButton.setStyle("-fx-base: lightblue;");
        runningButton.setStyle("-fx-base: lightgreen;");
        cyclingButton.setStyle("-fx-base: lightyellow;");
        swimmingButton.setStyle("-fx-base: lightpink;");

        // Set the gap between the graphic (image) and text in the buttons
        walkingButton.setGraphicTextGap(10);
        runningButton.setGraphicTextGap(10);
        cyclingButton.setGraphicTextGap(10);
        swimmingButton.setGraphicTextGap(10);

        // Set the text under the images
        walkingButton.setContentDisplay(ContentDisplay.TOP);
        runningButton.setContentDisplay(ContentDisplay.TOP);
        cyclingButton.setContentDisplay(ContentDisplay.TOP);
        swimmingButton.setContentDisplay(ContentDisplay.TOP);

        // Add buttons to the grid pane
        gridPane.add(walkingButton, 0, 0);
        gridPane.add(runningButton, 1, 0);
        gridPane.add(cyclingButton, 0, 1);
        gridPane.add(swimmingButton, 1, 1);

        // Add grid pane to the content
        content.getChildren().add(gridPane);

        return content;
    }

    private Button createBackButton() {
        Button backButton = new Button("Back");
        backButton.setAlignment(Pos.CENTER);

        backButton.setOnAction(backEvent -> {
            setContent(createContent());
        });

        return backButton;
    }

    private Button createActivityButton(String activity, ImageView imageView) {
        Button button = new Button(activity, imageView);
        button.setOnAction(event -> {
            // Create the buttons for overview and registration
            Button overviewButton = new Button("Overview");
            Button registerButton = new Button("Register");
            Button backButton = createBackButton();

            overviewButton.setOnAction(overviewEvent -> {
                // Handle overview button click
                createOverview(activity);
            });

            registerButton.setOnAction(registerEvent -> {
                // Handle register button click
                registerActivity(activity);
            });

            // VBox
            VBox buttonContainer = new VBox(10, overviewButton, registerButton, backButton);
            buttonContainer.setAlignment(Pos.CENTER);

            // Update content
            setContent(buttonContainer);
        });
        return button;
    }

    private void createOverview(String activity) {
        Button backButton = createBackButton();
        backButton.setAlignment(Pos.CENTER);
        VBox.setMargin(backButton, new Insets(10));

        VBox overviewContent = new VBox();
        overviewContent.getChildren().add(createOverviewContent(activity));
        VBox.setMargin(overviewContent, new Insets(10));

        VBox buttonContainer = new VBox(10, overviewContent, backButton);
        buttonContainer.setAlignment(Pos.CENTER);
        setContent(buttonContainer);

        selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                ListView<String> listView = findListView(overviewContent);
                listView.setItems(getActivityEntriesAsStringList(activity));
            }
        });
    }

    private void registerActivity(String activity) {
        // Create the registration form
        if (activity.equals("Walking")) {
            Label timeLabel = new Label("Time (minutes):");
            TextField timeField = new TextField();

            Label distanceLabel = new Label("Distance (meters):");
            TextField distanceField = new TextField();

            Button registerButton = new Button("Register");
            Button backButton = createBackButton();

            registerButton.setOnAction(e -> {
                String timeText = inputHandler.validateDouble(timeField.getText(), "time");
                String distanceText = inputHandler.validateDouble(distanceField.getText(), "distance");

                if (!timeText.isEmpty() && !distanceText.isEmpty()) {
                    double time = Double.parseDouble(timeText);
                    double distance = Double.parseDouble(distanceText);
                    LocalDateTime now = LocalDateTime.now();

                    // Create the activity entry
                    ActivityEntry entry = new ActivityEntry(activity, time, distance, now);
                    activityEntries.add(entry);

                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Activity Registered");
                    alert.setHeaderText(null);
                    alert.setContentText("Activity registered successfully!");
                    alert.showAndWait();
                    setContent(createContent());
                }
            });

            // Create the registration form layout
            GridPane gridPane = new GridPane();
            gridPane.setAlignment(Pos.CENTER);
            gridPane.setHgap(10);
            gridPane.setVgap(10);
            gridPane.setPadding(new Insets(10));

            gridPane.add(timeLabel, 0, 0);
            gridPane.add(timeField, 1, 0);
            gridPane.add(distanceLabel, 0, 1);
            gridPane.add(distanceField, 1, 1);
            gridPane.add(backButton, 0, 2);
            gridPane.add(registerButton, 1, 2);

            GridPane.setHalignment(registerButton, HPos.CENTER);
            GridPane.setHalignment(backButton, HPos.CENTER);

            // Align labels and fields horizontally
            ColumnConstraints columnConstraints = new ColumnConstraints();
            columnConstraints.setHgrow(Priority.ALWAYS);
            gridPane.getColumnConstraints().addAll(new ColumnConstraints(), columnConstraints);

            // Update content
            setContent(gridPane);
        } else if (activity.equals("Running") || activity.equals("Cycling")) {
            Label timeLabel = new Label("Time (minutes):");
            TextField timeField = new TextField();

            Label distanceLabel = new Label("Distance (meters):");
            TextField distanceField = new TextField();

            Label heartRateLabel = new Label("Heartrate (bpm)");
            TextField heartRateField = new TextField();

            Button registerButton = new Button("Register");
            Button backButton = createBackButton();

            registerButton.setOnAction(e -> {
                String timeText = inputHandler.validateDouble(timeField.getText(), "time");
                String distanceText = inputHandler.validateDouble(distanceField.getText(), "distance");
                String heartRateText = inputHandler.validateDouble(heartRateField.getText(), "heartrate");

                if (!timeText.isEmpty() && !distanceText.isEmpty()) {
                    double time = Double.parseDouble(timeText);
                    double distance = Double.parseDouble(distanceText);
                    LocalDateTime now = LocalDateTime.now();
                    double heartRate = Double.parseDouble(heartRateText);

                    // Create the activity entry
                    ActivityEntry entry = new RunningCyclingEntry(activity, time, distance, now, heartRate);
                    activityEntries.add(entry);

                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Activity Registered");
                    alert.setHeaderText(null);
                    alert.setContentText("Activity registered successfully!");
                    alert.showAndWait();
                    setContent(createContent());
                }
            });

            // Create the registration form layout
            GridPane gridPane = new GridPane();
            gridPane.setAlignment(Pos.CENTER);
            gridPane.setHgap(10);
            gridPane.setVgap(10);
            gridPane.setPadding(new Insets(10));

            gridPane.add(timeLabel, 0, 0);
            gridPane.add(timeField, 1, 0);
            gridPane.add(distanceLabel, 0, 1);
            gridPane.add(distanceField, 1, 1);
            gridPane.add(heartRateLabel, 0, 2);
            gridPane.add(heartRateField, 1, 2);
            gridPane.add(backButton, 0, 3);
            gridPane.add(registerButton, 1, 3);

            GridPane.setHalignment(registerButton, HPos.CENTER);
            GridPane.setHalignment(backButton, HPos.CENTER);

            // Align labels and fields horizontally
            ColumnConstraints columnConstraints = new ColumnConstraints();
            columnConstraints.setHgrow(Priority.ALWAYS);
            gridPane.getColumnConstraints().addAll(new ColumnConstraints(), columnConstraints);

            // Update content
            setContent(gridPane);
        } else if (activity.equals("Swimming")) {
            Label timeLabel = new Label("Time (minutes):");
            TextField timeField = new TextField();

            Label swimLapsLabel = new Label("Swim Laps (lap length is 25 meters):");
            TextField swimLapsField = new TextField();

            Button registerButton = new Button("Register");
            Button backButton = createBackButton();

            registerButton.setOnAction(e -> {
                String timeText = inputHandler.validateDouble(timeField.getText(), "time");
                String swimLapsText = inputHandler.validateDouble(swimLapsField.getText(), "swim laps");

                if (!timeText.isEmpty() && !swimLapsText.isEmpty()) {
                    double time = Double.parseDouble(timeText);
                    double swimLaps = Double.parseDouble(swimLapsText);
                    LocalDateTime now = LocalDateTime.now();
                    double distance = swimLaps * 25;

                    // Create the activity entry
                    ActivityEntry entry = new SwimmingActivityEntry(activity, time, distance, now, swimLaps);
                    activityEntries.add(entry);

                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Activity Registered");
                    alert.setHeaderText(null);
                    alert.setContentText("Activity registered successfully!");
                    alert.showAndWait();
                    setContent(createContent());
                }
            });

            // Create the registration form layout
            GridPane gridPane = new GridPane();
            gridPane.setAlignment(Pos.CENTER);
            gridPane.setHgap(10);
            gridPane.setVgap(10);
            gridPane.setPadding(new Insets(10));

            gridPane.add(timeLabel, 0, 0);
            gridPane.add(timeField, 1, 0);
            gridPane.add(swimLapsLabel, 0, 1);
            gridPane.add(swimLapsField, 1, 1);
            gridPane.add(backButton, 0, 3);
            gridPane.add(registerButton, 1, 3);

            GridPane.setHalignment(registerButton, HPos.CENTER);
            GridPane.setHalignment(backButton, HPos.CENTER);

            // Align labels and fields horizontally
            ColumnConstraints columnConstraints = new ColumnConstraints();
            columnConstraints.setHgrow(Priority.ALWAYS);
            gridPane.getColumnConstraints().addAll(new ColumnConstraints(), columnConstraints);

            // Update content
            setContent(gridPane);
        }
    }

    private Node createOverviewContent(String activity) {
        ListView<String> listView = new ListView<>();
        listView.setItems(getActivityEntriesAsStringList(activity));
        return listView;
    }

    private ObservableList<String> getActivityEntriesAsStringList(String activity) {
        ObservableList<String> entries = FXCollections.observableArrayList();

        // Filter and sort the activity entries
        List<ActivityEntry> filteredEntries = activityEntries.stream()
                .filter(entry -> entry.getActivity().equals(activity))
                .sorted(Comparator.comparing(ActivityEntry::getRegistrationTime).reversed())
                .collect(Collectors.toList());

        for (ActivityEntry entry : filteredEntries) {
            String entryString;
            if (entry instanceof SwimmingActivityEntry) {
                entryString = String.format(
                        "Swim Laps: %.2f laps | Time: %.2f minutes | Distance: %.2f meters |  | Registered at: %s",
                        entry.getSwimLaps(), entry.getTime(), entry.getDistance(), entry.getDateAsString());
            } else if (entry instanceof RunningCyclingEntry) {
                entryString = String.format(
                        "Tempo: %.2f km/h | Time: %.2f minutes | Distance: %.2f meters | Heartrate: %.2f bpm | Registered at: %s",
                        entry.getTempo(), entry.getTime(), entry.getDistance(), entry.getHeartRate(), entry.getDateAsString());
            } else {
                entryString = String.format(
                        "Tempo: %.2f km/h | Time: %.2f minutes | Distance: %.2f meters | Registered at: %s",
                        entry.getTempo(), entry.getTime(), entry.getDistance(), entry.getDateAsString());
            }
            entries.add(entryString);
        }

        return entries;
    }

    @SuppressWarnings("unchecked")
    private ListView<String> findListView(Parent parent) {
        for (Node child : parent.getChildrenUnmodifiable()) {
            if (child instanceof ListView) {
                return (ListView<String>) child;
            } else if (child instanceof Parent) {
                ListView<String> listView = findListView((Parent) child);
                if (listView != null) {
                    return listView;
                }
            }
        }
        return null;
    }
}