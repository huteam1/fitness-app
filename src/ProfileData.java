import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

public class ProfileData {
    private String name;
    private String surname;
    private String birthdate;
    private String gender;
    private String device;
    private double weight;
    private double length;
    private double VO2Max;

    public ProfileData() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getBirthdateAsString() {
        SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat outputDateFormat = new SimpleDateFormat("dd/MM/yyyy");

        try {
            Date date = inputDateFormat.parse(birthdate);
            return outputDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public int getAge() {
        LocalDate today = LocalDate.now();
        LocalDate birthday = convertToLocalDate(birthdate);
        Period period = Period.between(birthday, today);
        return period.getYears();
    }

    public LocalDate convertToLocalDate(String birthdate) {
        SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date date = inputDateFormat.parse(birthdate);
            return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getBMI() {
        double BMI = weight / (length * length);
        DecimalFormat df = new DecimalFormat("#.00");
        String formattedBMI = df.format(BMI);
        return Double.parseDouble(formattedBMI);
    }

    public double getVO2Max() {
        return VO2Max;
    }

    public void setVO2Max(double VO2Max) {
        this.VO2Max = VO2Max;
    }
}