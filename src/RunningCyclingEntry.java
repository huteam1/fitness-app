import java.time.LocalDateTime;

public class RunningCyclingEntry extends ActivityEntry {
    private double heartRate;

    public RunningCyclingEntry(String activity, double time, double distance, LocalDateTime registrationTime, double heartRate) {
        super(activity, time, distance, registrationTime);
        this.heartRate = heartRate;
    }

    @Override
    public double getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(double heartRate) {
        this.heartRate = heartRate;
    }
}
