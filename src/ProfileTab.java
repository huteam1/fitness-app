import java.time.LocalDate;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ProfileTab extends Tab {
    private ProfileData profileData;
    private VBox root;
    private VBox imageContainer;
    private GridPane content;
    private Stage primaryStage;
    private InputHandler inputHandler = new InputHandler();

    public ProfileTab(ProfileData profileData) {
        super("Profile");
        this.profileData = profileData;

        root = new VBox();
        root.setAlignment(Pos.CENTER);
        root.setSpacing(40);

        imageContainer = createImageContainer();
        content = createContent();

        // Add image container and content to the root VBox
        root.getChildren().addAll(imageContainer, content);

        // Set the root VBox as the content of the profiletab (Dit heb ik dus gedaan zodat ik de profiel icoon kon bewaren en zodat deze de gridpane niet zou betasten.)
        setContent(root);
    }

    private VBox createImageContainer() {
        VBox imageContainer = new VBox();
        imageContainer.setAlignment(Pos.CENTER);

        // Add Imageview for the profile icon
        ImageView userImage = new ImageView(new Image("img/user.png"));
        userImage.setFitWidth(75);
        userImage.setFitHeight(75);

        // Add picture to VBox
        imageContainer.getChildren().add(userImage);

        return imageContainer;
    }

    private GridPane createContent() {
        GridPane content = new GridPane();
        content.setAlignment(Pos.CENTER);
        content.setVgap(10);

        int rowIndex = 0;

        content.addRow(rowIndex++, createLabel("Name: ", profileData.getName()));
        content.addRow(rowIndex++, createLabel("Surname: ", profileData.getSurname()));
        content.addRow(rowIndex++, createLabel("Age: ", String.valueOf(profileData.getAge())));
        content.addRow(rowIndex++, createLabel("Gender: ", profileData.getGender()));
        content.addRow(rowIndex++, createLabel("Device: ", profileData.getDevice()));
        content.addRow(rowIndex++, createLabel("Weight: ", String.valueOf(profileData.getWeight()) + " kg"));
        content.addRow(rowIndex++, createLabel("Length: ", String.valueOf(profileData.getLength()) + " meter"));
        content.addRow(rowIndex++, createLabel("BMI: ", String.format("%.2f", profileData.getBMI())));
        content.addRow(rowIndex++, createLabel("VO2Max: ", String.valueOf(profileData.getVO2Max())));

        // Add edit buttons with correct spacing so they are right next to the according variables
        Button editNameButton = createEditButton("Edit");
        editNameButton.setOnAction(event -> handleEditButton("Name", profileData.getName()));
        content.add(editNameButton, 2, 0);

        Button editSurnameButton = createEditButton("Edit");
        editSurnameButton.setOnAction(event -> handleEditButton("Surname", profileData.getSurname()));
        content.add(editSurnameButton, 2, 1);

        Button editAgeButton = createEditButton("Edit");
        editAgeButton.setOnAction(event -> handleEditBirthdateButton(primaryStage));
        content.add(editAgeButton, 2, 2);

        Button editGenderButton = createEditButton("Edit");
        editGenderButton.setOnAction(event -> handleEditButton("Gender", profileData.getGender()));
        content.add(editGenderButton, 2, 3);

        Button editDeviceButton = createEditButton("Edit");
        editDeviceButton.setOnAction(event -> handleEditButton("Device", profileData.getDevice()));
        content.add(editDeviceButton, 2, 4);

        Button editWeightButton = createEditButton("Edit");
        editWeightButton.setOnAction(event -> handleEditButton("Weight", String.valueOf(profileData.getWeight())));
        content.add(editWeightButton, 2, 5);

        Button editLengthButton = createEditButton("Edit");
        editLengthButton.setOnAction(event -> handleEditButton("Length", String.valueOf(profileData.getLength())));
        content.add(editLengthButton, 2, 6);

        Button editVO2MaxButton = createEditButton("Edit");
        editVO2MaxButton.setOnAction(event -> handleEditButton("VO2Max", String.valueOf(profileData.getVO2Max())));
        content.add(editVO2MaxButton, 2, 8);

        // Add spacing for the edit buttons (Ze waren anders echt vlak naast de values van de variabelen)
        Insets buttonInsets = new Insets(0, 0, 0, 10);
        GridPane.setMargin(editNameButton, buttonInsets);
        GridPane.setMargin(editSurnameButton, buttonInsets);
        GridPane.setMargin(editAgeButton, buttonInsets);
        GridPane.setMargin(editGenderButton, buttonInsets);
        GridPane.setMargin(editDeviceButton, buttonInsets);
        GridPane.setMargin(editWeightButton, buttonInsets);
        GridPane.setMargin(editLengthButton, buttonInsets);
        GridPane.setMargin(editVO2MaxButton, buttonInsets);

        return content;
    }

    private Label createLabel(String labelText, String valueText) {
        Label label = new Label(labelText + valueText);
        label.setAlignment(Pos.CENTER_LEFT);
        label.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        return label;
    }

    private Button createEditButton(String buttonText) {
        Button button = new Button(buttonText);
        button.setMinWidth(60);
        return button;
    }

    public void handleEditButton(String field, String value) {
        // Create a new stage for the popup dialog
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Edit " + field);
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initOwner(primaryStage);

        // Create a grid pane for the dialog content
        GridPane dialogContent = new GridPane();
        dialogContent.setHgap(10);
        dialogContent.setVgap(10);
        dialogContent.setPadding(new Insets(10));

        // Create a text field for entering the new value
        TextField newValueField = new TextField(value);
        dialogContent.addRow(0, new Label("New " + field + ":"), newValueField);

        // Create a save button
        Button saveButton = new Button("Save");
        saveButton.setOnAction(event -> {
            String newValue = newValueField.getText();
            if (!newValue.isEmpty()) {
                switch (field) {
                    case "Name":
                        profileData.setName(newValue);
                        break;
                    case "Surname":
                        profileData.setSurname(newValue);
                        break;
                    case "Gender":
                        profileData.setGender(newValue);
                        break;
                    case "Device":
                        profileData.setDevice(newValue);
                        break;
                    case "Weight":
                        profileData.setWeight(Double.parseDouble(inputHandler.validateDouble(newValue, "Weight")));
                        break;
                    case "Length":
                        profileData.setLength(Double.parseDouble(inputHandler.validateDouble(newValue, "Length")));
                        break;
                    case "VO2Max":
                        profileData.setVO2Max(Double.parseDouble(inputHandler.validateDouble(newValue, "VO2Max")));
                        break;
                }
                updateContent();
                dialogStage.close();
            } else {
                showAlert(AlertType.ERROR, "Invalid " + field, "Please enter a valid " + field.toLowerCase() + ".");
            }
        });

        dialogContent.addRow(1, saveButton);

        dialogStage.setScene(new Scene(dialogContent));
        dialogStage.show();
    }

    private void updateContent() {
        VBox root = (VBox) getContent();
        GridPane newContent = createContent();
        root.getChildren().set(1, newContent);

        // Preserve the imagecontainer
        VBox imageContainer = (VBox) root.getChildren().get(0);
        root.getChildren().clear();
        root.getChildren().addAll(imageContainer, newContent);
    }

    private void showAlert(AlertType alertType, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    private void handleEditBirthdateButton(Stage parentStage) {
        // New stage for the pop up
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Edit Birthdate");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.initOwner(parentStage);

        // Grid pane for the dialog content
        GridPane dialogContent = new GridPane();
        dialogContent.setHgap(10);
        dialogContent.setVgap(10);
        dialogContent.setPadding(new Insets(10));

        // DatePicker for birthdate
        DatePicker birthdatePicker = new DatePicker();
        dialogContent.addRow(0, new Label("New Birthdate:"), birthdatePicker);

        // Save button
        Button saveButton = new Button("Save");
        saveButton.setOnAction(event -> {
            LocalDate birthdate = birthdatePicker.getValue();
            if (birthdate != null) {
                profileData.setBirthdate(birthdatePicker.getValue().toString());
                profileData.getAge(); // Getter for updated age
                updateContent();
                dialogStage.close();
            } else {
                showAlert(AlertType.ERROR, "Invalid Birthdate", "Please select a valid birthdate.");
            }
        });

        dialogContent.addRow(1, saveButton);

        dialogStage.setScene(new Scene(dialogContent));
        dialogStage.show();
    }
}