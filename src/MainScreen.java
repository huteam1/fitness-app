import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;

public class MainScreen {
    public void createTabs(Stage primaryStage, ProfileData profileData) {
        // Create the tabs
        ProfileTab profileTab = new ProfileTab(profileData);
        CoachTab coachesTab = new CoachTab(primaryStage);
        ActivityTab activitiesTab = new ActivityTab(primaryStage);
        HealthTab healthTab = new HealthTab(primaryStage, profileData);

        // Add the tabs to the TabPane
        TabPane tabPane = new TabPane();
        tabPane.getTabs().addAll(profileTab, activitiesTab, coachesTab, healthTab);
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

        // Set up the primary stage
        Scene scene = new Scene(tabPane, 400, 600);
        primaryStage.setTitle("Fitness App");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}