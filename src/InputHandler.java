import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class InputHandler {

    public String validateDouble(String input, String fieldName) {
        // Check if the input is a valid double
        try {
            Double.parseDouble(input);
            return input;
        } catch (NumberFormatException e) {
            showInvalidInputAlert(fieldName);
            return null;
        }
    }

    public void showInvalidInputAlert(String fieldName) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Invalid Input");
        alert.setHeaderText(null);
        alert.setContentText("Invalid input for " + fieldName + ". Please enter a valid value.");
        alert.showAndWait();
    }
}