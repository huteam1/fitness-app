import java.time.LocalDateTime;


public class SwimmingActivityEntry extends ActivityEntry {

    private double swimLaps;

    public SwimmingActivityEntry(String activity, double time, double distance, LocalDateTime registrationTime, double swimLaps) {
        super(activity, time, distance, registrationTime);
        this.swimLaps = swimLaps;
    }

    @Override
    public double getSwimLaps() {
        return swimLaps;
    }

    @Override
    public double getDistance() {
        double distance = swimLaps * 25;
        return distance;
    }

}