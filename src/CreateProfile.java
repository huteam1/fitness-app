import java.time.format.DateTimeParseException;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class CreateProfile {

    TextField nameField;
    TextField surnameField;
    DatePicker birthdatePicker;
    TextField genderField;
    TextField deviceField;
    TextField weightField;
    TextField lengthField;
    TextField vo2MaxField;
    Button saveButton;
    ProfileData profileData = new ProfileData();
    InputHandler inputHandler = new InputHandler();

    public void start(Stage primaryStage) {

        VBox root = new VBox();
        root.setAlignment(Pos.CENTER);
        root.setSpacing(32);
        root.setPadding(new Insets(10));

        // Welcome message
        Label welcomeLabel = new Label("Welcome to the Fitness App!");
        welcomeLabel.setStyle("-fx-font-size: 20px; -fx-font-weight: bold;");
        root.getChildren().add(welcomeLabel);
        Label infoLabel = new Label("Please enter your information to continue:");
        root.getChildren().add(infoLabel);

        // Create grid pane
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        // Create input fields
        nameField = new TextField();
        surnameField = new TextField();
        birthdatePicker = new DatePicker();
        genderField = new TextField();
        deviceField = new TextField();
        weightField = new TextField();
        lengthField = new TextField();
        vo2MaxField = new TextField();

        // Create save button
        saveButton = new Button("Save");
        saveButton.setOnAction(event -> handleSaveButton(primaryStage));

        // Add input fields and save button to the grid pane
        gridPane.addRow(0, createLabel("Name:"), nameField);
        gridPane.addRow(1, createLabel("Surname:"), surnameField);
        gridPane.addRow(2, createLabel("Birthdate: (MM/dd/yyyy)"), birthdatePicker);
        gridPane.addRow(3, createLabel("Gender:"), genderField);
        gridPane.addRow(4, createLabel("Device:"), deviceField);
        gridPane.addRow(5, createLabel("Weight: (in kg)"), weightField);
        gridPane.addRow(6, createLabel("Length: (in meters)"), lengthField);
        gridPane.addRow(7, createLabel("VO2Max:"), vo2MaxField);

        HBox buttonBox = new HBox(saveButton);
        buttonBox.setAlignment(Pos.CENTER);
        gridPane.add(buttonBox, 0, 8, 2, 1);
        root.getChildren().add(gridPane);

        // Set up the primary stage
        primaryStage.setTitle("Fitness App");
        primaryStage.setScene(new Scene(root, 400, 600));
        primaryStage.show();
    }

    private Label createLabel(String text) {
        Label label = new Label(text);
        label.setAlignment(Pos.CENTER_RIGHT);
        return label;
    }

    private void handleSaveButton(Stage primaryStage) {
        try {
            String name = nameField.getText();
            String surname = surnameField.getText();
            String birthdate = birthdatePicker.getValue().toString();
            String gender = genderField.getText();
            String device = deviceField.getText();
            String weight = inputHandler.validateDouble(weightField.getText(), "Weight");
            String length = inputHandler.validateDouble(lengthField.getText(), "Length");
            String vo2Max = inputHandler.validateDouble(vo2MaxField.getText(), "VO2Max");

            // Check if any textfields for Strings are empty, I've already handled empty
            // date or doubles by other means
            if (name.isEmpty() || surname.isEmpty() || gender.isEmpty() || device.isEmpty()) {
                showErrorMessage("Please fill in all the fields.");
                return;
            }

            profileData.setName(name);
            profileData.setSurname(surname);
            profileData.setBirthdate(birthdate);
            profileData.setGender(gender);
            profileData.setDevice(device);
            profileData.setWeight(Double.parseDouble(weight));
            profileData.setLength(Double.parseDouble(length));
            profileData.setVO2Max(Double.parseDouble(vo2Max));

            MainScreen mainScreen = new MainScreen();
            mainScreen.createTabs(primaryStage, profileData);

        } catch (NumberFormatException e) {
            showErrorMessage("Invalid number format. Please enter a valid number.");
        } catch (IllegalArgumentException e) {
            showErrorMessage("Invalid input.");
        } catch (DateTimeParseException e) {
            showErrorMessage("Please enter a valid birthdate.");
        } catch (NullPointerException e) {
            showErrorMessage("Please enter a valid birthdate");
        }
    }

    private void showErrorMessage(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }
}