import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ActivityEntry {
    private String activity;
    private double tempo;
    private double time;
    private double distance;
    private LocalDateTime registrationTime;
    private double imaginary;

    public ActivityEntry(String activity, double time, double distance, LocalDateTime registrationTime) {
        this.activity = activity;
        this.tempo = (distance / 1000) / (time / 60.0);
        this.time = time;
        this.distance = distance;
        this.registrationTime = registrationTime;
    }

    public double getSwimLaps() {
        return imaginary;
    }

    public double getHeartRate() {
        return imaginary;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public double getTempo() {
        return tempo;
    }

    public void setTempo(double tempo) {
        this.tempo = tempo;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public LocalDateTime getRegistrationTime() {
        return registrationTime;
    }

    public void setRegistrationTime(LocalDateTime registrationTime) {
        this.registrationTime = registrationTime;
    }

    public String getDateAsString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        return registrationTime.format(formatter);
    }
}