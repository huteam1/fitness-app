import java.io.InputStream;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class HealthTab extends Tab {
    private ProfileData profileData;
    private VBox content;
    private Label weightLabel;
    Stage primaryStage;

    public HealthTab(Stage primaryStage, ProfileData profileData) {
        super("Health");
        this.primaryStage = primaryStage;
        this.profileData = profileData;
        setContent(createContent());
    }

    private VBox createContent() {
        content = new VBox();
        content.setSpacing(10);

        Label titleLabel = new Label("Health");
        titleLabel.setAlignment(Pos.TOP_CENTER);
        titleLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

        weightLabel = new Label("Weight: " + profileData.getWeight() + " kg");
        weightLabel.setStyle("-fx-font-size: 14px;");

        Button editButton = new Button("Edit");
        editButton.setOnAction(e -> handleEditButton());

        HBox weightBox = new HBox(weightLabel, editButton);
        weightBox.setAlignment(Pos.CENTER_LEFT);
        weightBox.setSpacing(10);

        Label adviceLabel = new Label();
        adviceLabel.setWrapText(true);
        adviceLabel.setStyle("-fx-font-size: 14px;");

        content.getChildren().addAll(titleLabel, weightBox, adviceLabel);

        calculateAdvice(adviceLabel);

        return content;
    }

    private void calculateAdvice(Label adviceLabel) {
        double jongAankomen = 18.5 * profileData.getLength() * profileData.getLength();
        double jongAankomenVerschil = Math.round((jongAankomen - profileData.getWeight()) * 100) / 100.0;
        double jongAfvallen = 24.99 * profileData.getLength() * profileData.getLength();
        double jongAfvallenVerschil = Math.round((profileData.getWeight() - jongAfvallen) * 100) / 100.0;
        double oudAankomen = 22 * profileData.getLength() * profileData.getLength();
        double oudAankomenVerschil = Math.round((oudAankomen - profileData.getWeight()) * 100) / 100.0;
        double oudAfvallen = 27.99 * profileData.getLength() * profileData.getLength();
        double oudAfvallenVerschil = Math.round((profileData.getWeight() - oudAfvallen) - 100) / 100.0;

        StringBuilder adviceBuilder = new StringBuilder();

        if (profileData.getAge() < 70) {
            if (profileData.getBMI() < 17.0) {
                adviceBuilder.append(
                        "Your weight is too low. This is not good for your health. Contact your doctor to determine the possible cause.\n");
                adviceBuilder.append("You need to gain at least ").append(jongAankomenVerschil)
                        .append(" kg for a healthy weight.\n");
                AddThumbDown();
            } else if (17.0 <= profileData.getBMI() && profileData.getBMI() < 18.5) {
                adviceBuilder.append("Your weight is slightly too low. You may benefit from gaining some weight.\n");
                adviceBuilder.append("You need to gain at least ").append(jongAankomenVerschil)
                        .append(" kg for a healthy weight.\n");
                AddThumbsUp();
            } else if (18.5 <= profileData.getBMI() && profileData.getBMI() < 24.99) {
                adviceBuilder.append("Your weight is within the healthy range.\n");
                AddThumbsUp();
            } else if (25.0 <= profileData.getBMI() && profileData.getBMI() < 27.99) {
                adviceBuilder.append("Your weight is slightly too high. You may benefit from losing some weight.\n");
                adviceBuilder.append("You need to lose at least ").append(jongAfvallenVerschil)
                        .append(" kg for a healthy weight.\n");
                AddThumbsUp();
            } else if (27.99 <= profileData.getBMI()) {
                adviceBuilder.append(
                        "Your weight is too high. This is not good for your health. Contact your doctor to determine the possible cause.\n");
                adviceBuilder.append("You need to lose at least ").append(jongAfvallenVerschil)
                        .append(" kg for a healthy weight.\n");
                AddThumbDown();
            }
        } else {
            if (profileData.getBMI() < 23.0) {
                adviceBuilder.append(
                        "Your weight is too low. This is not good for your health. Contact your doctor to determine the possible cause.\n");
                adviceBuilder.append("You need to gain at least ").append(oudAankomenVerschil)
                        .append(" kg for a healthy weight.\n");
                AddThumbDown();
            } else if (23.0 <= profileData.getBMI() && profileData.getBMI() < 27.99) {
                adviceBuilder.append("Your weight is within the healthy range.\n");
                AddThumbsUp();
            } else if (28.0 <= profileData.getBMI() && profileData.getBMI() < 32.99) {
                adviceBuilder.append("Your weight is slightly too high. You may benefit from losing some weight.\n");
                adviceBuilder.append("You need to lose at least ").append(oudAfvallenVerschil)
                        .append(" kg for a healthy weight.\n");
                AddThumbsUp();
            } else if (32.99 <= profileData.getBMI()) {
                adviceBuilder.append(
                        "Your weight is too high. This is not good for your health. Contact your doctor to determine the possible cause.\n");
                adviceBuilder.append("You need to lose at least ").append(oudAfvallenVerschil)
                        .append(" kg for a healthy weight.\n");
                AddThumbDown();
            }
        }

        adviceLabel.setText(adviceBuilder.toString());
    }

    private void handleEditButton() {
        Stage dialogStage = new Stage(StageStyle.UTILITY);
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        dialogStage.setResizable(false);
        dialogStage.setTitle("Edit Weight");

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setPadding(new Insets(10));
        grid.setHgap(10);
        grid.setVgap(10);

        Label weightLabel = new Label("Weight (kg):");
        TextField weightField = new TextField(String.valueOf(profileData.getWeight()));

        grid.add(weightLabel, 0, 0);
        grid.add(weightField, 1, 0);

        Button saveButton = new Button("Save");
        saveButton.setOnAction(e -> {
            double weight = Double.parseDouble(weightField.getText());
            profileData.setWeight(weight);
            this.weightLabel.setText("Weight: " + weight + " kg"); // Update the displayed weight label
            calculateAdvice((Label) content.getChildren().get(2));
            dialogStage.close();
        });

        Button cancelButton = new Button("Cancel");
        cancelButton.setOnAction(e -> dialogStage.close());

        HBox buttonBox = new HBox(saveButton, cancelButton);
        buttonBox.setAlignment(Pos.CENTER);
        buttonBox.setSpacing(10);

        VBox dialogContent = new VBox(grid, buttonBox);
        dialogContent.setSpacing(10);
        dialogContent.setPadding(new Insets(10));

        Scene dialogScene = new Scene(dialogContent);
        dialogStage.setScene(dialogScene);
        dialogStage.show();
    }

    private void AddThumbsUp() {
        InputStream imageStream = getClass().getResourceAsStream("img/thumbs-up.png");
        Image thumbsUpImage = new Image(imageStream);
        ImageView thumbsUpImageView = new ImageView(thumbsUpImage);
        thumbsUpImageView.setFitHeight(40);
        thumbsUpImageView.setFitWidth(40);
        content.getChildren().add(thumbsUpImageView);
    }

    private void AddThumbDown() {
        InputStream imageStream = getClass().getResourceAsStream("img/thumb-down.png");
        Image thumbDownImage = new Image(imageStream);
        ImageView thumbDownImageView = new ImageView(thumbDownImage);
        thumbDownImageView.setFitHeight(40);
        thumbDownImageView.setFitWidth(40);
        content.getChildren().add(thumbDownImageView);
    }
}
